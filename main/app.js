function addTokens(input, tokens){
    let sir=input;
    if( typeof input !=="string"){
        throw new Error("Invalid input");
    }
    else if(input.length<6) {
        throw new Error("Input should have at least 6 characters");
    }
    else{
         for(let i in tokens){
             if(typeof tokens[i].tokenName!=='string' )
             {
                 throw new Error("Invalid array format");
             }
             else{
                if(input.indexOf('...'))
                {
                    sir=sir.replace('...','$'+'{'+tokens[i].tokenName+'}');
                }
                else{
                    return input;
                }
                }
            }
         return sir;
        }

}

const app = {
    addTokens: addTokens
}

module.exports = app;

//console.log(addTokens('Subsemnatul ', [{tokenName: 'subsemnatul'}]))